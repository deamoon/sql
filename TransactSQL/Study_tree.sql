// ���������
Select b
From T
Where b not in(select e From T)

// ������� ��������
Select e
From T
except
Select b
From T

//copy � FULL �� ������ (������ �����)

Insert into FULL
Select F1.b, F2.e
From Full as F1 inner join FULL as F2 on F1.e=F2.b 
  
Insert into FULL
Select F1.b, F2.e
From Full as F1 inner join FULL as F2 on F1.e=F2.b 
  left join FULL as F3 on F3.b=F1.b and F3.e=F2.e and F3.v=F1.v+F2.v //>=
Where F3.b is NULL

with Tab(b, e) as
(
  Select b,e
  From T
  Union
  Select T1.b, T2.e
  From Tab as T1 inner join T as T2 on T1.e=T2.b
)

Select *
From Tab