DECLARE @A TABLE (id1 int, id2 int)
INSERT INTO @A VALUES (1,2), (2,3), (3,4)

DECLARE @B TABLE (id1 int, id2 int)
INSERT INTO @B VALUES (1,NULL), (2,4), (2,3)

-- NULL-��������
SELECT *
FROM @B
WHERE id2 is NULL

-- �������� ��������� 2 �������.
SELECT *
FROM @B
WHERE id2 is not NULL

SELECT *
FROM @B
WHERE NOT id2 is NULL

SELECT *
FROM @A
WHERE id1 in (SELECT id2 FROM @B)

-- ������ ����������, ��� � NULL ����� ���������� ����� ���������.
SELECT *
FROM @A
WHERE id1 not in (SELECT id2 FROM @B)

-- ���������� ������� ���������� NULL ��������
SELECT COUNT(*), COUNT(id1), COUNT(distinct id1), COUNT(id2), SUM(id2)
FROM @B

-- ���������� � ����������� ������
-- ���������� ��������� ���� �������� ����������, �� ������� ������������ ������ ���������.
SELECT *
FROM @A A INNER JOIN @B B ON
	A.id1 = B.id1

SELECT *
FROM @A A, @B B 
WHERE A.id1 = B.id1

SELECT *
FROM @A A LEFT JOIN @B B ON
	A.id1 = B.id1
	
SELECT *
FROM @A A RIGHT JOIN @B B ON
	A.id1 = B.id1

-- �������� ���������� ��������� ���� ��������:
SELECT *
FROM @A A LEFT JOIN @B B ON
	A.id1 = B.id1 AND
	B.id1 = 1
	
SELECT *
FROM @A A LEFT JOIN @B B ON
	A.id1 = B.id1
WHERE B.id1 = 1
	
-- ������� ���� ������ �� �������� id1
SELECT *
FROM @A A LEFT JOIN @B B ON
	A.id1 = B.id1
WHERE B.id1 is NULL

-- ���������� �� ����������� (���������, ��� ��������� ��� ������� �������� id1 � ������� � ���������� ����� � ������� B, �������� id1 � ������� ������, ��� A.id1)
SELECT *
FROM @A A INNER JOIN @B B ON
	A.id1 >= B.id1
	
-- ���������� �� ����������� 
-- (���������, ��� ��������� ��� ������� �������� id1 � ������� � ���������� ����� � ������� B, 
-- �������� id1 � ������� ������, ��� A.id1)
SELECT A.id1, COUNT(B.id1) as count
FROM @A A LEFT JOIN @B B ON
	A.id1 > B.id1
GROUP BY A.id1