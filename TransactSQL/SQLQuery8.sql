  DECLARE @n int, @c float, @n1 int, @n2 int
  SET @n  = 3
  SET @c  = 2
  SET @n1 = 1
  SET @n2 = 4
DECLARE @D TABLE (row int, col int, value int)
INSERT INTO @D VALUES (1,1,1), (1,2,3), (2,1,2), (2,2,1)

-- ������� B (n2) + X = A (n1)
  IF (
    SELECT COUNT(distinct row) FROM matrix1 WHERE id = @n1
  ) = (
    SELECT COUNT(distinct row) FROM matrix1 WHERE id = @n2 
  ) AND (
    SELECT COUNT(distinct col) FROM matrix1 WHERE id = @n1
  ) = (
    SELECT COUNT(distinct col) FROM matrix1 WHERE id = @n2
  )  
  
  SELECT M1.row, M1.col, M1.value - M2.value 
  FROM matrix1 M1 INNER JOIN matrix1 M2 ON
      M1.id = @n1 AND M2.id = @n2 AND M1.col = M2.col AND M1.row = M2.row
  ORDER BY M1.row, M1.col
  
  ELSE SELECT '������'

-- ������� ��������� � ������������� �������� A (n1) * X = B (n2)
  IF (
    SELECT COUNT(distinct row) FROM matrix1 WHERE id = @n1
  ) = (
    SELECT COUNT(distinct row) FROM matrix1 WHERE id = @n2
  )   
  
  SELECT M1.col, M2.col, SUM(M1.value * M2.value)
  FROM matrix1 M1 INNER JOIN matrix1 M2 ON
      M1.id = @n1 AND M2.id = @n2 AND M1.row = M2.row
  GROUP BY M1.col, M2.col    
  ORDER BY M1.col, M2.col
  
  ELSE SELECT '������'

-- ��������� ������ A (n1) * B (n2)
  IF (
    SELECT COUNT(distinct col) FROM matrix1 WHERE id = @n1
  ) = (
    SELECT COUNT(distinct row) FROM matrix1 WHERE id = @n2
  )   
  
  SELECT M1.row, M2.col, SUM(M1.value * M2.value)
  FROM matrix1 M1 INNER JOIN matrix1 M2 ON
      M1.id = @n1 AND M2.id = @n2 AND M1.col = M2.row
  GROUP BY M1.row, M2.col    
  ORDER BY M1.row, M2.col
  
  ELSE SELECT '������'

-- ����� �� ����������� ��� ������� A (n1) * B (n2)?
  IF (
    SELECT COUNT(distinct col)
    FROM matrix1
    WHERE id = @n1
  ) = (
    SELECT COUNT(distinct row)
    FROM matrix1
    WHERE id = @n2
  )  
  SELECT '��' ELSE SELECT '���'

-- �������� ������ A (n1) + B (n2)
  IF (
    SELECT COUNT(distinct row) FROM matrix1 WHERE id = @n1
  ) = (
    SELECT COUNT(distinct row) FROM matrix1 WHERE id = @n2 
  ) AND (
    SELECT COUNT(distinct col) FROM matrix1 WHERE id = @n1
  ) = (
    SELECT COUNT(distinct col) FROM matrix1 WHERE id = @n2
  )  
  
  SELECT M1.row, M1.col, M1.value + M2.value 
  FROM matrix1 M1 INNER JOIN matrix1 M2 ON
      M1.id = @n1 AND M2.id = @n2 AND M1.col = M2.col AND M1.row = M2.row
  ORDER BY M1.row, M1.col
  
  ELSE SELECT '������'

-- ����� �� ������� ��� ������� A (n1) + B (n2)?
  IF (
    SELECT COUNT(distinct row)
    FROM matrix1
    WHERE id = @n1
  ) = (
    SELECT COUNT(distinct row)
    FROM matrix1
    WHERE id = @n2
  ) AND (
    SELECT COUNT(distinct col)
    FROM matrix1
    WHERE id = @n1
  ) = (
    SELECT COUNT(distinct col)
    FROM matrix1
    WHERE id = @n2
  )  
  SELECT '��' ELSE SELECT '���'

-- ������� D, ������� �������� ������-������� ������� @n
SELECT M.row, M.col, M.value
FROM matrix1 M INNER JOIN @D D ON
    (M.row = D.value AND D.row = 1 OR M.col = D.value AND D.row = 2) AND M.id = @n
GROUP BY M.row, M.col, D.col, M.value
HAVING COUNT(M.row) = 2

-- ����������� �������� ����� � ������ �������� ������� @n
SELECT row - row/2 as row, col/2 as col, value
FROM matrix1
WHERE  row % 2 = 1 AND col % 2 = 0 AND id = @n

-- ��������� ������� @n �� ������������
  IF (
    SELECT COUNT(distinct row)
    FROM matrix1
    WHERE id = @n
  ) = (
    SELECT COUNT(distinct col)
    FROM matrix1
    WHERE id = @n
  )
  SELECT '��' ELSE SELECT '���'
  
-- ��������� ������� @n �� ������
  IF (
    SELECT COUNT(distinct row)
    FROM matrix1
    WHERE id = @n
  ) = 1 OR (
    SELECT COUNT(distinct col)
    FROM matrix1
    WHERE id = @n
  ) = 1
  SELECT '��' ELSE SELECT '���'

-- �������� �� ����� @c ������� @n 
  SELECT row, col, value * @c value
  FROM matrix1
  WHERE id = @n 

-- ��������� �� �������������� ������� @n
IF (
  SELECT COUNT(*)
  FROM matrix1 T1, matrix1 T2
  WHERE T1.id = @n AND T2.id = @n AND T1.col = T2.row AND T1.row = T2.col AND T1.value = T2.value
) = (
  SELECT COUNT(*)
  FROM matrix1
  WHERE id = @n
)
  PRINT '��' ELSE PRINT '���'

-- ������� ����������������� ������� @n
SELECT col as row, row as col, value
FROM matrix1
WHERE id = @n
ORDER BY row, col

-- ������� ������� @n
SELECT row, col, value
FROM matrix1
WHERE id = @n
ORDER BY row, col

