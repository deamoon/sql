SET dateformat dmy

DECLARE @Tovar TABLE (tovar_id int primary key, name nchar(100), price int)
INSERT INTO @Tovar
VALUES (1,'��������',20), (2,'��������',25), (3,'�����',15)

DECLARE @Pokupateli TABLE (pokupatel_id int primary key, name nchar(100))
INSERT INTO @Pokupateli
VALUES (1,'�������'), (2,'�����'), (3,'������')

DECLARE @Cheki TABLE (ndoc_id int primary key, data datetime, pokupatel_id  int, summa int)
--INSERT INTO @Cheki
--VALUES (1,'04.03.13',2,40),(2,'05.03.13',2,65),(3,'06.03.13',1,10),(4,'07.03.13',3,35)

DECLARE @Cheki_dannie TABLE (ndoc_id int, tovar_id int, price int, kolvo int, primary key(ndoc_id, tovar_id))
--INSERT INTO @Cheki_dannie
--VALUES (1,2,5,4),(1,3,8,2),(2,1,2,3),(2,2,7,1),(2,3,15,2),(3,3,20,1)

Insert into @Cheki_dannie(ndoc_id, tovar_id, price, kolvo)
  Select P.pokupatel_id, 1, T.price, Floor(RAND()*5)+1
  From @Pokupateli as P, @Tovar as T
  Where T.tovar_id=1
  UNION
  Select P.pokupatel_id, 2, T.price, Floor(RAND()*5)+1
  From @Pokupateli as P, @Tovar as T
  Where T.tovar_id=2
  UNION
  Select P.pokupatel_id, 3, T.price, Floor(RAND()*5)+1
  From @Pokupateli as P, @Tovar as T
  Where T.tovar_id=3

Insert into @Cheki(ndoc_id, data, pokupatel_id, summa)
  Select T.ndoc_id, CAST(GETDATE() as date), T.ndoc_id, Sum(T.price*T.kolvo)
  From @Cheki_dannie as T
  Group by T.ndoc_id

Update @Cheki_dannie
  Set price = price*1.5
  Where tovar_id = (Select MIN(tovar_id) From @Cheki_dannie)

Update @Cheki
  Set summa = P.sum_price
  From ( Select SUM(T.price*T.kolvo) as sum_price, T.ndoc_id as P_ndoc_id
         From @Cheki_dannie as T
         Group by T.ndoc_id
       ) as P
  Where ndoc_id = P.P_ndoc_id

INSERT INTO @Cheki
VALUES (4,'04.03.13',3,50),(5,'04.03.13',3,50), (6,'05.03.13',1,50),(7,'06.03.13',2,50)

Select T2.name
From ( Select distinct pokupatel_id
       From @Cheki
       Where summa > ( Select AVG(summa)
                       From @Cheki
                       Where MONTH(data)=3 AND YEAR(data)=2013
                     )
     ) as T1 inner join @Pokupateli as T2 ON T1.pokupatel_id = T2.pokupatel_id       

Select P2.name as Name, P1.kol as Rating
From ( Select T1.id as T1_id, COUNT(T2.id) as kol
       From ( Select pokupatel_id as id, SUM(summa) as pok_sum
              From @Cheki     
              Where MONTH(data)=3 AND YEAR(data)=2013
              Group by pokupatel_id
            ) as T1 inner join 
            ( Select pokupatel_id as id, SUM(summa) as pok_sum
              From @Cheki     
              Where MONTH(data)=3 AND YEAR(data)=2013
              Group by pokupatel_id
            ) as T2 ON T1.pok_sum<=T2.pok_sum
       Group by T1.id
     ) as P1 inner join @Pokupateli as P2 ON P1.T1_id = P2.pokupatel_id
Order by P1.kol