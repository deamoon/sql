DECLARE @T TABLE (x float, y float)
INSERT INTO @T
VALUES (0,0), (0.1,0.01), (0.2,0.035), (0.3,0.1), (0.4,0.18), (0.5,0.3), (0.6,0.4), (0.7,0.46), (0.8,0.64), (0.9,0.85), (1,1.2), (1.1,1.21),
(1.2,1.5), (1.3,1.6), (1.4,1.9), (1.5,2.35), (1.6,2.45), (1.7,2.8), (1.8,3.3), (1.9,3.7), (2,4.1), (2.1,4.3), (2.2,4.75), (2.3,5.2),
(2.4,5.9), (2.5,6.4), (2.6,6.4), (2.7,7.2), (2.8,7.9), (2.9,8.5) , (3,9)

SELECT COUNT(T1.y) as �����, T1.y as ������� 
FROM @T T1 INNER JOIN @T T2 ON
    T1.y >= T2.y
GROUP BY T1.y    
HAVING ((SELECT COUNT(y) FROM @T)+1)/2 = COUNT(T1.y)

SELECT (AVG(LOG(x)*LOG(y)) - AVG(LOG(x))*AVG(LOG(y)))/(AVG(LOG(x)*LOG(x))-AVG(LOG(x))*AVG(LOG(x)))
FROM @T
WHERE x<>0 AND y<>0

SELECT SUM(log(x)*log(y))
FROM @T
WHERE x<>0 AND y<>0

SELECT COUNT(x)
FROM @T
WHERE x>1 AND y<5

SELECT MAX(x), MIN(x), MAX(y), MIN(y), AVG(x), AVG(y), COUNT(x)
FROM @T

