DECLARE @Student TABLE (id int primary key, FIO nchar(100))
INSERT INTO @Student
VALUES (1,'���������� ������� ���������'), (2,'�������� ����� �����������'), (3,'��������� ��������� ������������')

DECLARE @Study TABLE (id int primary key, name nchar(100))
INSERT INTO @Study
VALUES (1,'���'), (2,'����'), (3,'��')

DECLARE @Pos TABLE (data datetime, id_study int, id_student int)
INSERT INTO @Pos
VALUES ('04.03.13',1,3), ('05.03.13',1,1), ('02.02.13',1,2), ('02.01.13',2,2)

Select W1.FIO
From @Student W1 left join (
  Select distinct T1.id
  From @Student T1 inner join @Pos T2 ON T1.id=T2.id_student inner join @Study T3 ON T3.id=T2.id_study
  Where T2.data > '01.03.13' AND T3.id=1
) W2 ON W1.id=W2.id 
Where IsNULL(W2.id,-1)=-1

Select W1.FIO
From @Student W1 left join (
  Select distinct T1.id
  From @Student T1 inner join @Pos T2 ON T1.id=T2.id_student
  Where T2.data > '01.03.13'
  GROUP BY T1.id
) W2 ON W1.id=W2.id 
Where IsNULL(W2.id,-1)=-1

Select T1.id, COUNT(distinct T2.id_study) as ���_��
From @Student T1 inner join @Pos T2 ON T1.id=T2.id_student
GROUP BY T1.id
Order By 2 desc

Select T1.id, T2.data, COUNT(T2.id_study) as ���_��
From @Student T1 inner join @Pos T2 ON T1.id=T2.id_student
GROUP BY T1.id, T2.data

Select COUNT(id) as ���_��
From @Student
Where FIO not LIKE '���%' 