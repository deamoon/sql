Create Table T(id int, val int unique)
--Create Table T(id int primary key, val int)
--Create Table T(id int, val int, primary key(id, val))
--Create Table T(id int, val int, constraint my_val default(0))
--Create Table T(id int, val int, check(val<5 or id>0))
Drop Table T
Create Table T(id int primary key, val int)
Create Table T1(id1 int, field int, id2 int references T(id)) --������, ������� ������ T1

Create Index MyIndex on T
(
  id asc,
  val asc
) 

--Alter Table T add column field float
Alter Table T add field float

Insert into T(id, val)
 Select id1, field
 From T1
 
Delete 
 From T
 Where id=1 -- �������� � ��������� ����
 
Truncate Table T -- ������ ��������

Delete
 From T
 From T inner join T1 on T.id=T1.id1

Update T
Set val=5+val
Where T.id=2

Update T
Set val=NULL, id=3
Where T.id=2

Update T
Set T.val=T1.field
From T inner join T1 on T.id=T1.id1

Select *
From T
where exists(
  Select *
  From T1
  Where T1.id1=T.id
  )