DECLARE @T TABLE (id1 int, id2 int)
INSERT INTO @T VALUES
(1,1), (1,20), (1,3), (2,3), (3,4)

-- ���� ��������: ����� �������� �������� ��������, ��� ������ ����������, � �������� ��������� � ����������.
-- ���������� �� ���������� (����� ���������� ��������� � ����������)
SELECT id1 as id2, id2 as id1
FROM @T
ORDER BY id1, id2

-- ���������� �� �����������
SELECT id1 as ����1, id2 as ����2
FROM @T
ORDER BY ����1, ����2

-- ���������� �� ������ �������
SELECT id1 as id2, id2 as id1
FROM @T
ORDER BY 1, 2

-- ���������� � ������ ������� (asc �� ���������, desc ���������)
SELECT id1 as id2, id2 as id1
FROM @T
ORDER BY id1, id2 desc

-- ������ � Top � ������������
SELECT top 2 id1
FROM @T
ORDER BY 1

SELECT top 2 with ties id1 
FROM @T
ORDER BY 1

SELECT top 2 id1
FROM @T
ORDER BY id2 desc -- ��������, ��� ����������� ����� �� ��������������� ����. ������ ��?

-- distinct � Top
SELECT distinct top 2 id1
FROM @T
ORDER BY 1

-- ������� ������ (����������� ������� ������, ����������, ��� ����� ����������)
SELECT *
FROM @T
WHERE id1 in (1,2) AND id2 between 1 and 3

-- ������� ������ ��� ���������� ������
SELECT *
FROM @T T1, @T T2
WHERE T1.id1 <= T2.id1 AND T1.id2 = T2.id2 

-- ����������� (��������, ��� ������� AVG)
SELECT id1, SUM(id2) T1, COUNT(*) T2, AVG(id2) T3
FROM @T
GROUP BY id1

-- ��� ���������?
SELECT id1, SUM(id2) T1, COUNT(*) T2, AVG(id2) T3
FROM @T
GROUP BY id1, id2

SELECT SUM(id2), MAX(id1)
FROM @T

-- �������� 2 ��������� �������, ���� �� �������? ��� ����� �������� � HAVING?
SELECT id1, SUM(id2) T1, COUNT(*) T2, AVG(id2) T3
FROM @T
GROUP BY id1
HAVING id1 = 1

SELECT id1, SUM(id2) T1, COUNT(*) T2, AVG(id2) T3
FROM @T
WHERE id1 = 1
GROUP BY id1

-- ����� �� ��������� ������� �� HAVING � WHERE? ����������, ��� ��������� �� HAVING ����������� � SELECT
SELECT id1, SUM(id2) T1, COUNT(*) T2, AVG(id2) T3
FROM @T
GROUP BY id1
HAVING MAX(id2) = 3