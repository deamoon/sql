DECLARE @City TABLE (id int primary key, name nchar(100), id_area int)
INSERT INTO @City
VALUES (1,'������',2), (2,'�������',1), (3,'�����������',3), (4,'�������',1), (5,'������-���������',1), (6,'������',4), (7,'������������',2), (8,'��������',4)

DECLARE @Area TABLE (id int primary key, name nchar(100))
INSERT INTO @Area
VALUES (1,'���������'), (2,'����������'), (3,'�������������'), (4,'��������')

DECLARE @Rout TABLE (id1 int, id2 int, distance  int, primary key(id1, id2))
INSERT INTO @Rout
VALUES (1,3,7),(1,7,1),(3,6,1),(5,6,4),(4,5,1),(2,4,2),(1,8,2),(3,8,2)

DECLARE @Time TABLE (number int primary key, id int)
INSERT INTO @Time
VALUES (2,1),(3,3),(4,6),(1,7),(6,4),(5,5)

DECLARE @W TABLE(dist int, kol_real int, kol int)
Insert Into @W
Select SUM(distance), COUNT(distance), (Select Max(number)-MIN(number) From @Time)
From  ( Select T1.id as id1, T2.id as id2
		From ( Select number, id
			   From @Time
			   Where number!=(Select MAX(number) From @Time)
			 ) T1 inner join
			 ( Select number, id
			   From @Time
			   Where number!=(Select MIN(number) From @Time)
			 ) T2 ON T1.number + 1 = T2.number
      ) P1 inner join 
      ( Select id1, id2, distance
        From @Rout
        Union
        Select id2 as id1, id1 as id2, distance
        From @Rout
      ) P2 ON P1.id1=P2.id1 AND P1.id2=P2.id2      
if (Select kol_real From @W)<(Select kol From @W) Select '��� ����' as ���������� ELSE (Select dist as ���������� From @W)
       
Select T1.id2 as id1, T2.id2 as id2
From (Select id1, id2, distance From @Rout UNION Select id2 as id1, id1 as id2, distance From @Rout) T1 inner join 
     (Select id1, id2, distance From @Rout UNION Select id2 as id1, id1 as id2, distance From @Rout) T2 
      ON T1.id1=T2.id1 AND T1.id2<T2.id2
Where T1.distance + T2.distance < (Select distance From @Rout as R Where T1.id2=R.id1 AND T2.id2=R.id2)     

Select P1.id1, P1.id2
From ( Select T1.id as id1, T2.id as id2
       From (Select id From @City) T1 inner join (Select id From @City) T2 ON T1.id < T2.id
     ) P1 left join @Rout P2 ON P1.id1=P2.id1 AND P1.id2=P2.id2
Where IsNULL(P2.id1,-1)=-1 AND IsNULL(P2.id2,-1)=-1

Select top 1 with ties T2.name as '����� 1', T3.name as '����� 2', T1.distance
From (Select id1, id2, distance
      From @Rout
     ) T1 inner join @City T2 ON T1.id1=T2.id inner join @City T3 ON T1.id2=T3.id
Where T2.id_area = T3.id_area
Order by T1.distance

Select T2.name as '����� 1', T3.name as '����� 2'
From (Select id1, id2
      From @Rout
      Where distance = (Select MIN(distance) From @Rout)
     ) T1 inner join @City T2 ON T1.id1=T2.id inner join @City T3 ON T1.id2=T3.id